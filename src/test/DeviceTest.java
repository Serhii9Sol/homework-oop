package test;

import main.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class DeviceTest {

    private final ProcessorArm pa = new ProcessorArm(2_400_000_000L, 1000L, 1_000_000_000L);
    private final ProcessorX86 pX = new ProcessorX86(2_400_000_000L, 1000L, 1_000_000_000L);
    private final Memory memory = new Memory(5);
    private final Device cut = new Device(pa, memory);

    static Arguments[] readAllTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[0], 0),
                Arguments.arguments(new String[]{"0", "1"}, 2),
                Arguments.arguments(new String[]{"0", "1", "2", "3", "4"}, 5),
                Arguments.arguments(new String[]{"0", "1", "2", "3", "4"}, 7),
        };
    }

    @ParameterizedTest
    @MethodSource("readAllTestArgs")
    void readAllTest(String[] expected,int countOfSave){
        for (int i = 0; i < countOfSave; i++){
            memory.save(String.valueOf(i));
        }
        String[] actual = cut.readAll();

        assertArrayEquals(expected, actual);
    }

//    ********************************************

    static Arguments[] saveTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[0], new String[0]),
                Arguments.arguments(new String[]{"0", "1"}, new String[]{"0", "1"}),
                Arguments.arguments(new String[]{"0", "1", "2", "3", "4"}, new String[]{"0", "1", "2", "3", "4"}),
                Arguments.arguments(new String[]{"0", "1", "2", "3", "4"}, new String[]{"0", "1", "2", "3", "4", "5", "6"}),
        };
    }

    @ParameterizedTest
    @MethodSource("saveTestArgs")
    void saveTest(String[] expected, String[] saved) {
        cut.save(saved);
        String[] actual = cut.readAll();

        assertArrayEquals(expected, actual);
    }

//    ********************************************

    static Arguments[] dataProcessingTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"a", "s", "d", "f", "g"}, new String[]{"A", "S", "D", "F", "G"}),
                Arguments.arguments(new String[]{"a", "S", "D", "f", "g"}, new String[]{"A", "S", "D", "F", "G"}),
                Arguments.arguments(new String[]{"a", "s", "d", null, null}, new String[]{"A", "S", "D"}),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessingTestArgs")
    void dataProcessingTest(String[] saved, String[] expected) {
        cut.save(saved);
        cut.dataProcessing();
        String[] actual = cut.readAll();

        assertArrayEquals(expected, actual);
    }

//    ********************************************

    static Arguments[] getSystemInfoTestArgs(){
        return new Arguments[]{
                Arguments.arguments(new String[]{"a", "s", "d", "f", "g"},
                        "Processor{ architecture= ARM, frequency=2400000000, cache=1000, bitCapacity=1000000000}\n" +
                        "MemoryInfo{memory=5, occupiedMemory=100}"),
                Arguments.arguments(new String[]{"a", "s", "d", null, null},
                        "Processor{ architecture= ARM, frequency=2400000000, cache=1000, bitCapacity=1000000000}\n" +
                        "MemoryInfo{memory=5, occupiedMemory=60}"),
        };
    }

    @ParameterizedTest
    @MethodSource("getSystemInfoTestArgs")
    void getSystemInfoTest(String[] saved, String expected){
        cut.save(saved);
        String actual = cut.getSystemInfo();

        assertEquals(expected, actual);
    }
}