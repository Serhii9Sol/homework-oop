package main;

public class Memory {
    private String[] memoryCell;

    public Memory(int capacityOfMemory) {
        this.memoryCell = new String[capacityOfMemory];
    }

    public String readLast() {
        for(int i = memoryCell.length - 1; i >= 0; i--){
            if(memoryCell[i] != null){
                return memoryCell[i];
            }
        }
        return null;
    }

    public String removeLast(){
        String buffer = null;
        for(int i = memoryCell.length - 1; i >= 0; i--){
            if(memoryCell[i] != null){
                buffer = memoryCell[i];
                memoryCell[i] = null;
                return buffer;
            }
        }
        return null;
    }

    public boolean save(String value){
        for(int i =  0; i < memoryCell.length; i++){
            if(memoryCell[i] == null){
                memoryCell[i] = value;
                return true;
            }
        }
        return false;
    }

    public MemoryInfo getMemoryInfo(){
        int allMemory = memoryCell.length;
        int freeMemory = 0;
        for(int i =  0; i < allMemory; i++){
            if(memoryCell[i] == null){
                freeMemory++;
            }
        }
        return new MemoryInfo(allMemory, 100 * (allMemory - freeMemory) /  allMemory);
    }

}
